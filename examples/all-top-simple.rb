require 'hakuban'
require 'socket'
require_relative './cpu-usage.rb'


exchange = Hakuban::Exchange.new
connector = Hakuban::WebsocketConnector.new(exchange, "ws://localhost:3001")


Thread.new {
	previous_cpu_usage_state = nil
	my_utilization = exchange.object_expose_contract(["utilizations"],{host: Socket.gethostname, process: $$}).build
	my_utilization.for_each_till_next { |state_sink|
		loop {
			previous_cpu_usage_state, cpu_usage = CPU::current_usage(previous_cpu_usage_state)
			state_sink.send Hakuban::ObjectState.new({"percentage"=>cpu_usage}).json_serialize  if cpu_usage
			sleep 1
		}
	}
}


all_utilizations = exchange.tag_observe_contract("utilizations").build
all_utilizations.for_each_concurrent { |state_stream|
	state_stream.for_each { |state|
		puts "#{state_stream.descriptor.json["host"]}:#{state_stream.descriptor.json["process"]} -> #{state.json_deserialize.data["percentage"]}"
	}
}
