module CPU

	def self.current_usage(previous_cpu_usage_state)
		current_uptime = open("/proc/uptime") { |file| file.read.split(" ")[0].to_f }
		current_cpu_ticks = open("/proc/stat") { |file| 
			file.read.split("\n").select { |line| line.start_with?(/cpu[0-9]+/) }.map { |line| line.split(" ")[4].to_f }
		}
		cpu_usage = previous_cpu_usage_state&.dig(0)&.zip(current_cpu_ticks)&.map { |old_ticks, new_ticks|
			if new_ticks - old_ticks == 0
				0
			else
				[0,(100 - (new_ticks - old_ticks) / (current_uptime - previous_cpu_usage_state[1]))].max.floor 
			end	
		}
		[[current_cpu_ticks, current_uptime], cpu_usage]
	end

end


