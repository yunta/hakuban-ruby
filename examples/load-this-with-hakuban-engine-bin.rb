require 'hakuban/engine'
require_relative './cpu-usage.rb'



class ObserveCPUUtilization < ::Hakuban::Engine

	def contract(exchange)
		exchange.tag_observe_contract("utilizations").build
	end

	def handle_with_thread(state_stream)
		state_stream.for_each { |state|
			puts "#{state_stream.descriptor.json} -> #{state.data}"
		}
	end

end
 



class ExposeCPUUtilization < ::Hakuban::Engine

	def initialize
		@id = { host: Socket.gethostname, process: $$ }
	end


	def contract(exchange)
		exchange.object_expose_contract(["utilizations"],@id).build
	end


	def handle_with_thread(state_sink)
		puts "Start publishing CPU stats of #{@id}..."
		previous_cpu_usage_state = nil
		loop {
			previous_cpu_usage_state, cpu_usage = CPU::current_usage(previous_cpu_usage_state)
			state_sink.send Hakuban::ObjectState.new({"percentage"=>cpu_usage}).json_serialize  if cpu_usage
			sleep 1
		}
	rescue Stop
		puts "... stop publishing, nobody's interested"
	end

end
 




