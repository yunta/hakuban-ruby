require_relative "lib/hakuban/version"

Gem::Specification.new do |spec|
  spec.name          = "hakuban"
  spec.version       = Hakuban::VERSION
  spec.authors       = ["yunta"]
  spec.email         = ["maciej.blomberg@mikoton.com"]
  spec.summary       = "Ruby binding for Hakuban library"
  spec.description   = "Ruby binding for convenient data-object sharing library - Hakuban."
  spec.homepage      = "https://gitlab.com/yunta/hakuban-ruby"
  spec.license       = "MIT"

  spec.required_ruby_version = Gem::Requirement.new("~> 3.0")

  spec.files = Dir["{bin,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  spec.bindir        = "bin"
  spec.executables   = ["hakuban-observe", "hakuban-engine"]
  spec.require_paths = ["lib"]

  spec.add_development_dependency "rspec", "~>3.13"
  spec.add_development_dependency "simplecov", "~>0.22"
  spec.add_runtime_dependency "ffi", "~>1.17"
  spec.add_runtime_dependency "json", "~>2.9"
  spec.add_runtime_dependency "slop", "~>4.10"
end
