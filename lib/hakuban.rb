# frozen_string_literal: true

module Hakuban

	def self.hakuban_initialize
		require_relative 'hakuban/ffi.rb'
	end

end


require_relative 'hakuban/contract.rb'
require_relative 'hakuban/descriptor.rb'
require_relative "hakuban/engine.rb"
require_relative 'hakuban/exchange.rb'
require_relative 'hakuban/ffi-object.rb'
require_relative 'hakuban/logger.rb'
require_relative 'hakuban/object_state_sink.rb'
require_relative 'hakuban/object_state_stream.rb'
require_relative 'hakuban/object_state.rb'
require_relative 'hakuban/tokio-websocket-connector.rb'
require_relative "hakuban/version"


#TODO: error classes
