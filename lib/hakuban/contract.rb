require 'hakuban/ffi-object.rb'
require 'hakuban/stream.rb'


module Hakuban

	class Contract < FFIObject

		attr_reader :descriptor

		def inspect
			"#<#{self.class.name} #{descriptor} #{self.dropped? ? "DROPPED" : ""}>"
		end

	end

	class ObserveContract < Contract; end
	class ExposeContract < Contract; end

	class ObjectObserveContract < ObserveContract
		
		private_class_method :new

		def initialize(exchange, descriptor, &block)
			super()
			@exchange, @descriptor = exchange, descriptor
			Thread.handle_interrupt(Object => :never) {
				@exchange.with_pointer { |exchange_pointer|
					@descriptor.with_pointer { |descriptor_pointer|
						initialize_pointer(FFI::hakuban_object_observe_contract_new(exchange_pointer, descriptor_pointer),:hakuban_object_observe_contract_drop,nil)
					}
				}
				do_and_drop_or_return(&block)
			}
		end

	public

		include Stream

		def next(&block)
			process_item(
				lambda { |pointer| FFI::hakuban_object_observe_contract_next(pointer) },
				lambda { |pointer| FFIObject::from_ffi_pointer(ObjectStateStream, pointer) },
				&block
			)
		end


	end


	class ObjectExposeContract < ExposeContract

		private_class_method :new

		def initialize(exchange, descriptor, capacity, &block)
			super()
			@exchange, @descriptor = exchange, descriptor
			Thread.handle_interrupt(Object => :never) {
				@exchange.with_pointer { |exchange_pointer|
					@descriptor.with_pointer { |descriptor_pointer|
						initialize_pointer(FFI::hakuban_object_expose_contract_new(exchange_pointer, descriptor_pointer, capacity),:hakuban_object_expose_contract_drop,nil)
					}
				}
				do_and_drop_or_return(&block)
			}
		end

	public 

		include Stream

		def next(&block)
			process_item(
				lambda { |pointer| FFI::hakuban_object_expose_contract_next(pointer) },
				lambda { |pointer| FFIObject::from_ffi_pointer(ObjectStateSink, pointer) },
				&block
			)
		end

	end


	class TagObserveContract < ObserveContract

		private_class_method :new

		def initialize(exchange, descriptor, &block)
			super()
			@exchange, @descriptor = exchange, descriptor
			Thread.handle_interrupt(Object => :never) {
				@exchange.with_pointer { |exchange_pointer|
					@descriptor.with_pointer { |descriptor_pointer|
						initialize_pointer(FFI::hakuban_tag_observe_contract_new(exchange_pointer, descriptor_pointer),:hakuban_tag_observe_contract_drop,nil)
					}
				}
				do_and_drop_or_return(&block)
			}
		end

	public
	
		include Stream

		def next(&block)
			process_item(
				lambda { |pointer| FFI::hakuban_tag_observe_contract_next(pointer) },
				lambda { |pointer| FFIObject::from_ffi_pointer(ObjectStateStream, pointer) },
				&block
			)
		end

	end


	class TagExposeContract < ExposeContract

		private_class_method :new

		def initialize(exchange, descriptor, capacity, &block)
			super()
			@exchange, @descriptor = exchange, descriptor
			Thread.handle_interrupt(Object => :never) {
				@exchange.with_pointer { |exchange_pointer|
					@descriptor.with_pointer { |descriptor_pointer|
						initialize_pointer(FFI::hakuban_tag_expose_contract_new(exchange_pointer, descriptor_pointer, capacity),:hakuban_tag_expose_contract_drop,nil)
					}
				}
				do_and_drop_or_return(&block)
			}
		end

	public

		include Stream

		def next(&block)
			process_item(
				lambda { |pointer| FFI::hakuban_tag_expose_contract_next(pointer) },
				lambda { |pointer| FFIObject::from_ffi_pointer(ObjectStateSink, pointer) },
				&block				
			)
		end

	end
	
end