require 'set'

require 'hakuban/ffi-object.rb'


module Hakuban


	class ObjectDescriptor < FFIObject
		attr_reader :tags, :json

		def initialize(tags, json)
			super()
			Hakuban::hakuban_initialize
			@json = json.freeze
			@tags = Set.new(tags.map { |tag| tag.kind_of?(TagDescriptor) ? tag : TagDescriptor.new(tag) })
			FFIObject.with_pointers(@tags) { |tag_pointers|
				tag_pointers_array = ::FFI::MemoryPointer.new(:pointer, tag_pointers.size)
				tag_pointers_array.write_array_of_pointer(tag_pointers)
				initialize_pointer(FFI::hakuban_object_descriptor_new(@json.to_json,tag_pointers.size,tag_pointers_array).unwrap,:hakuban_object_descriptor_drop,:hakuban_object_descriptor_clone)
			}
		end

		def initialize_from_ffi_pointer(pointer)
			super(pointer, :hakuban_object_descriptor_drop,:hakuban_object_descriptor_clone)
			@json = JSON::parse(FFI::hakuban_object_descriptor_json(pointer).clone)
			tags_array = FFI::hakuban_object_descriptor_tags(pointer)
		 	@tags = tags_array[:pointer].read_array_of_pointer(tags_array[:length]).map { |tag_pointer|
				FFIObject.from_ffi_pointer(TagDescriptor, FFI::hakuban_tag_descriptor_clone(tag_pointer))
		 	}
		end

		def initialize_copy(original)
			super
			@tags = original.tags.map(&:clone)
		end

		def ==(other)
			@tags == other.tags and @json == other.json
		end

		alias eql? ==

		def hash
			[@tags.hash, @json.hash].hash
		end

		def inspect
			"#<ObjectDescriptor @tags={%s}, @json=%p>"%[self.tags.map(&:inspect).join(","), self.json]
		end

	end


	class TagDescriptor < FFIObject
		attr_reader :json

		def initialize(json)
			super()
			Hakuban::hakuban_initialize
			@json = json.freeze
			initialize_pointer(FFI::hakuban_tag_descriptor_new(json.to_json).unwrap, :hakuban_tag_descriptor_drop, :hakuban_tag_descriptor_clone)
		end

		def initialize_from_ffi_pointer(pointer)
			super(pointer, :hakuban_tag_descriptor_drop,:hakuban_tag_descriptor_clone)
			@json = JSON::parse(FFI::hakuban_tag_descriptor_json(pointer).clone)
		end

		def ==(other)
			@json == other.json
		end

		alias eql? ==

		def hash
			@json.hash
		end

		def inspect
			"#<TagDescriptor @json=%p>"%[self.json]
		end
	end

end
