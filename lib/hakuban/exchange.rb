require 'hakuban/ffi-object.rb'
require 'socket'

module Hakuban

	class Exchange < FFIObject

		def initialize(&block)
			super()
			Hakuban::hakuban_initialize
			Hakuban::logger_initialize("hakuban=warn", skip_if_already_initialized: true)
			Thread.handle_interrupt(Object => :never) {
				pointer = FFI::hakuban_exchange_new().unwrap
				initialize_pointer(pointer, :hakuban_exchange_drop, :hakuban_exchange_clone)
				self.do_and_drop_or_return(&block)
			}
		end

	public

		def object_observe_contract(tags, descriptor=nil)
			if tags.kind_of? ObjectDescriptor
				ObjectObserveContractBuilder.new(self, tags)
			else 
				ObjectObserveContractBuilder.new(self, ObjectDescriptor.new(tags, descriptor))
			end
		end

		def object_expose_contract(tags, descriptor=nil)
			if tags.kind_of? ObjectDescriptor
				ObjectExposeContractBuilder.new(self, tags)
			else 
				ObjectExposeContractBuilder.new(self, ObjectDescriptor.new(tags, descriptor))
			end
		end

		def tag_observe_contract(descriptor)
			if descriptor.kind_of? TagDescriptor
				TagObserveContractBuilder.new(self, descriptor)
			else
				TagObserveContractBuilder.new(self, TagDescriptor.new(descriptor))
			end
		end

		def tag_expose_contract(descriptor)
			if descriptor.kind_of? TagDescriptor
				TagExposeContractBuilder.new(self, descriptor)
			else
				TagExposeContractBuilder.new(self, TagDescriptor.new(descriptor))
			end
		end

	end


	class ObjectObserveContractBuilder

		def initialize(exchange, descriptor)
			@exchange, @descriptor = exchange, descriptor
		end

		def build(&block)
			ObjectObserveContract.send(:new, @exchange, @descriptor, &block)
		end

	end

	class ObjectExposeContractBuilder

		def initialize(exchange, descriptor)
			@exchange, @descriptor = exchange, descriptor
			@capacity = 1
		end

		def with_capacity(capacity)
			@capacity = capacity
		end

		def build(&block)
			ObjectExposeContract.send(:new, @exchange, @descriptor, @capacity, &block)
		end

	end


	class TagObserveContractBuilder

		def initialize(exchange, descriptor)
			@exchange, @descriptor = exchange, descriptor
		end

		def build(&block)
			TagObserveContract.send(:new, @exchange, @descriptor, &block)
		end

	end

	class TagExposeContractBuilder

		def initialize(exchange, descriptor)
			@exchange, @descriptor = exchange, descriptor
			@capacity = 1
		end

		def with_capacity(capacity)
			@capacity = capacity
		end

		def build(&block)
			TagExposeContract.send(:new, @exchange, @descriptor, @capacity, &block)
		end

	end

end