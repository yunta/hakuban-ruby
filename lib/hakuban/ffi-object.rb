require 'set'


module Hakuban

	class FFIObject

		class PointerAlreadyDropped < Exception; end

		def initialize
			@pointer_mutex = Mutex.new
		end

		private def initialize_from_ffi_pointer(pointer, drop_fn, clone_fn)
			@pointer_mutex = Mutex.new
			initialize_pointer(pointer, drop_fn, clone_fn)
		end


		def self.from_ffi_pointer(cls, pointer)
			cls.allocate.tap { |new_instance| new_instance.initialize_from_ffi_pointer(pointer) }
		end

		private def initialize_pointer(pointer, drop_fn, clone_fn)
			@pointer, @drop_fn, @clone_fn = pointer.address, drop_fn, clone_fn
			ObjectSpace.define_finalizer(self, FFIObject::generate_finalizer(drop_fn, @pointer))
		end
		

		def initialize_copy(original)
			Thread.handle_interrupt(Object => :never) {
				if @clone_fn.nil?
					@pointer_mutex.synchronize {
						raise PointerAlreadyDropped  if original.instance_variable_get(:@pointer).nil?
						original.instance_variable_set(:@pointer, nil)
						ObjectSpace.undefine_finalizer(original)
					}
				else
					original.with_pointer { |pointer|
						@pointer = FFI::method(@clone_fn).call(::FFI::Pointer.new(pointer)).address 
					}
				end
				@pointer_mutex = Mutex.new
				ObjectSpace.undefine_finalizer(self)
				ObjectSpace.define_finalizer(self, FFIObject::generate_finalizer(@drop_fn, @pointer))
			}
		end


		def with_pointer
			@pointer_mutex.synchronize {
				if @pointer
					yield ::FFI::Pointer.new(@pointer)
				else
					raise PointerAlreadyDropped
				end
			}
		end


		def self.with_pointers(objects,&original_block)
			do_locked = proc { |original_block, remaining_objects, pointers|
				if remaining_objects.size == 0
					original_block.call(pointers)
				else
					object, i = remaining_objects.shift
					object.with_pointer { |pointer|
						pointers[i] = pointer
						do_locked.call(original_block, remaining_objects, pointers)
					}
				end
			}
			do_locked.call(original_block, objects.each.with_index.sort_by { |object, i| object.instance_variable_get(:@pointer) || -i }, Array.new(objects.size))
		end


		def drop
			@pointer_mutex.synchronize {
				Thread.handle_interrupt(Object => :never) {
					if !!@pointer
						ObjectSpace.undefine_finalizer(self)
						FFI::method(@drop_fn).call(::FFI::Pointer.new(@pointer))
						@pointer = nil
					end
				}
			}
		end


		def dropped?
			!@pointer
		end


		def self.generate_finalizer(symbol, pointer_address)
			proc { |_|
				FFI::method(symbol).call(::FFI::Pointer.new(pointer_address)) 
			}
		end


		# this should always be called with interrupts disabled, in the same section where pointer creation occurs
		def do_and_drop_or_return(&block)
			if block
				begin
					Thread.handle_interrupt(Object => :immediate) {
						yield self
					}
				ensure
					self.drop
				end
			else
				self
			end
		end
		
		def inspect
			"#<#{self.class.name} #{self.dropped? ? "DROPPED" : "%016X"%@pointer}>"
		end

	end

end