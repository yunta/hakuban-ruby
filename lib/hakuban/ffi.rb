require 'json'
require 'ffi'
require 'hakuban/ffi-object'
require 'hakuban/refinements'

module Hakuban::FFI

	extend FFI::Library
	ffi_lib 'hakuban'


	class FFIError < Exception; end
	class FFIErrorInvalidString < FFIError; end
	class FFIErrorInvalidJSON < FFIError; end
	class FFIErrorInvalidURL < FFIError; end
	class FFIErrorInvalidLogLevel < FFIError; end
	class FFIErrorUnknownError < FFIError; end

	
	class FFIResultStatus < FFI::Struct
		layout :id, :uint8
		ENUM = [:Ok, :Pointer, :Pending, :EndOfStream, :InvalidString, :InvalidJSON, :InvalidURL, :InvalidLogLevel, :LoggerInitializationError, :ConnectionTerminated].freeze

		def to_sym
			ENUM[self[:id]]
		end

		def to_exception
			Hakuban::FFI::const_get("FFIError"+ENUM[self[:id]].to_s)
		end
	end


	class FFIResult < FFI::Struct
		layout :status, FFIResultStatus, :pointer, :pointer

		def unwrap
			return true  if self[:status].to_sym == :Ok
			return nil  if self[:status].to_sym == :EndOfStream
			raise self[:status].to_exception  if self[:status].to_sym != :Pointer
			self[:pointer]
		end

		def status
			self[:status].to_sym
		end
	end


	class FFIFuture
		
		using ThreadExt

		#This HAS to be called with interrupts disabled.
		#If both, internal exception, and an interrupt fire, pay attention not to drop one of them.
		def self.await(future_pointer)
			error = result_thread = result = nil
			future_pointer_for_blocking_await = Hakuban::FFI::hakuban_future_clone(future_pointer)
			begin
				# ThreadError gets raised here on process shutdown
				result_thread = Thread.new { Hakuban::FFI::hakuban_future_await(future_pointer_for_blocking_await) }
				Thread.handle_interrupt(Object => :immediate) { result_thread.join }
			rescue Object => e
				error = e
			end
			Hakuban::FFI::hakuban_future_drop(future_pointer)
			if result_thread
				result = result_thread.join_with_warning
			else 
				Hakuban::FFI::hakuban_future_drop(future_pointer_for_blocking_await)
			end
			return [result, error]
		end

	end


	class FFIArray < FFI::Struct
	 	layout :length, :size_t, :pointer, :pointer
	end


	# Every function which can cause a callback has to be "blocking: true".
	# Otherwise they are very likely to deadlock on GVL and other locks acquired by Connection(s) on incomming message
	# Fortunately, we don't use callbacks any more. And the only function with unbounded execution time is the future-await. So, only setting that one as blocking.
	
	callback :waker, [ :pointer ], :void

	attach_function :hakuban_logger_initialize, [ :string ], FFIResult.by_value

	attach_function :hakuban_exchange_new, [ ], FFIResult.by_value
	attach_function :hakuban_exchange_drop, [ :pointer ], :void
	attach_function :hakuban_exchange_clone, [ :pointer ], :pointer

	attach_function :hakuban_tokio_init_multi_thread, [ :size_t ], :pointer
	attach_function :hakuban_tokio_websocket_connector_new, [ :pointer, :pointer, :string ], FFIResult.by_value
	attach_function :hakuban_tokio_websocket_connector_drop, [ :pointer ], :void

	attach_function :hakuban_object_observe_contract_new, [ :pointer, :pointer ], :pointer
	attach_function :hakuban_object_observe_contract_drop, [ :pointer ], :void
	attach_function :hakuban_object_observe_contract_next, [ :pointer ], :pointer

	attach_function :hakuban_object_expose_contract_new, [ :pointer, :pointer, :uint32 ], :pointer
	attach_function :hakuban_object_expose_contract_drop, [ :pointer ], :void
	attach_function :hakuban_object_expose_contract_next, [ :pointer ], :pointer

	attach_function :hakuban_tag_observe_contract_new, [ :pointer, :pointer ], :pointer
	attach_function :hakuban_tag_observe_contract_drop, [ :pointer ], :void
	attach_function :hakuban_tag_observe_contract_next, [ :pointer ], :pointer

	attach_function :hakuban_tag_expose_contract_new, [ :pointer, :pointer, :uint32 ], :pointer
	attach_function :hakuban_tag_expose_contract_drop, [ :pointer ], :void
	attach_function :hakuban_tag_expose_contract_next, [ :pointer ], :pointer

	attach_function :hakuban_object_state_stream_drop, [ :pointer ], :void
	attach_function :hakuban_object_state_stream_next, [ :pointer ], :pointer
	attach_function :hakuban_object_state_stream_descriptor, [ :pointer ], :pointer

	attach_function :hakuban_object_state_sink_drop, [ :pointer ], :void
	attach_function :hakuban_object_state_sink_next, [ :pointer ], :pointer
	attach_function :hakuban_object_state_sink_send, [ :pointer, :pointer ], :pointer
	attach_function :hakuban_object_state_sink_descriptor, [ :pointer ], :pointer

	attach_function :hakuban_object_state_sink_params_drop, [ :pointer ], :void
	attach_function :hakuban_object_state_sink_params_clone, [ :pointer ], :pointer

	attach_function :hakuban_object_state_new, [ :size_t, :pointer, :size_t, :pointer, :size_t, :pointer, :uint64 ], FFIResult.by_value
	attach_function :hakuban_object_state_drop, [ :pointer ], :void
	attach_function :hakuban_object_state_clone, [ :pointer ], :pointer
	attach_function :hakuban_object_state_data, [ :pointer ], FFIArray.by_value
	attach_function :hakuban_object_state_format, [ :pointer ], FFIArray.by_value
	attach_function :hakuban_object_state_version, [ :pointer ], FFIArray.by_value
	attach_function :hakuban_object_state_synchronized_ago, [ :pointer ], :uint64

	attach_function :hakuban_tag_descriptor_new, [ :string ], FFIResult.by_value
	attach_function :hakuban_tag_descriptor_drop, [ :pointer ], :void
	attach_function :hakuban_tag_descriptor_clone, [ :pointer ], :pointer
	attach_function :hakuban_tag_descriptor_json, [ :pointer ], :string

	attach_function :hakuban_object_descriptor_new, [ :string, :size_t, :pointer ], FFIResult.by_value
	attach_function :hakuban_object_descriptor_drop, [ :pointer ], :void
	attach_function :hakuban_object_descriptor_clone, [ :pointer ], :pointer
	attach_function :hakuban_object_descriptor_json, [ :pointer ], :string
	attach_function :hakuban_object_descriptor_tags, [ :pointer ], FFIArray.by_value

	attach_function :hakuban_future_clone, [ :pointer ], :pointer
	attach_function :hakuban_future_drop, [ :pointer ], :void
	#attach_function :hakuban_future_poll, [ :pointer, :waker, :pointer ], FFIResult.by_value  #this is pita to get working right with interrupts, and without leaking memory in weird cases
	attach_function :hakuban_future_await, [ :pointer ], FFIResult.by_value, blocking: true

	attach_function :hakuban_array_drop, [ FFIArray.by_value ], :void

	FFIResultStatus.freeze
	FFIResult.freeze
	FFIFuture.freeze
	freeze
end
