module Hakuban

	@@logger_initialized = false

	def self.logger_initialize(default_level, skip_if_already_initialized: false)
		Hakuban::hakuban_initialize
		if @@logger_initialized and !skip_if_already_initialized
			raise "Logger already initialized. This can't be done more than once. Make sure logger_initialize is called before any Exchange gets constructed."  
		end
		if not @@logger_initialized
			raise "Invalid default log level string"  if FFI::hakuban_logger_initialize(default_level).status != :Ok
			@@logger_initialized = true
		end
	end

end