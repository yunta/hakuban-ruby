require 'hakuban/ffi-object.rb'



module Hakuban

	class ObjectState < FFIObject

		class WrongFormatError < Exception; end

		def initialize(data, version: nil, format: nil, synchronized_us_ago: 0, &block)
			super()
			if version.nil?
				timestamp = Time.new
				version = [0, timestamp.to_i, timestamp.nsec]
			end
			@version = version
			@format = [format || []].flatten
			@synchronized_us_ago = synchronized_us_ago
			@data = data
			@pointer = false
			Thread.handle_interrupt(Object => :never) {
				self.do_and_drop_or_return(&block)
			}
		end

		def initialize_from_ffi_pointer(pointer)
			super(pointer, :hakuban_object_state_drop, :hakuban_object_state_clone)
		end



		def with_pointer
			@pointer_mutex.synchronize {
				if @pointer == false
					Hakuban::hakuban_initialize
					version_pointer = ::FFI::MemoryPointer.new(:int64, @version.size)
					version_pointer.write_array_of_int64(@version)
					format_pointer = ::FFI::MemoryPointer.new(:pointer, @format.size)
					format_strings = format.map {|string| ::FFI::MemoryPointer.from_string(string)}
					format_pointer.write_array_of_pointer(format_strings)
					data_pointer = ::FFI::MemoryPointer.from_string(@data)
					pointer = FFI::hakuban_object_state_new(@version.size, version_pointer, @format.size, format_pointer, @data.bytesize, data_pointer, @synchronized_us_ago).unwrap
					initialize_pointer(pointer, :hakuban_object_state_drop, :hakuban_object_state_clone)
				end
				yield ::FFI::Pointer.new(@pointer)
			}
		end

		def data
			return @data  if defined? @data
			array = with_pointer { |pointer| FFI::hakuban_object_state_data(pointer) }
			#array[:data_bytes].read_array_of_char(array[:length]) #.pack('c*').clone()
			@data = array[:pointer].read_string(array[:length]).clone()
		end

		def format
			return @format  if defined? @format
			array = with_pointer { |pointer| FFI::hakuban_object_state_format(pointer) }
			@format = array[:pointer].read_array_of_pointer(array[:length]).map { |string| string.read_string().clone() }
		end

		def version
			return @version  if defined? @version
			array = with_pointer { |pointer| FFI::hakuban_object_state_version(pointer) }
			@version = array[:pointer].read_array_of_int64(array[:length])
		end

		def synchronized_us_ago
			return @synchronized_us_ago  if defined? @synchronized_us_ago
			@synchronized_us_ago = with_pointer { |pointer| FFI::hakuban_object_state_synchronized_ago(pointer) }
		end

		def with_data(data)
			ObjectState.new(data, version: version, format: format, synchronized_us_ago: synchronized_us_ago)
		end

		def with_version(version)
			ObjectState.new(data, version: version, format: format, synchronized_us_ago: synchronized_us_ago)
		end

		def with_format(format)
			ObjectState.new(data, version: version, format: format, synchronized_us_ago: synchronized_us_ago)
		end

		def with_synchronized_us_ago(synchronized_us_ago)
			ObjectState.new(data, version: version, format: format, synchronized_us_ago: synchronized_us_ago)
		end

		def json_deserialize
			raise WrongFormatError  if format[-1] != "JSON"
			ObjectState.new(JSON.parse(data), version: version, format: format[0...-1], synchronized_us_ago: synchronized_us_ago)
		end

		def json_serialize
			ObjectState.new(data.to_json, version: version, format: format+["JSON"], synchronized_us_ago: synchronized_us_ago)
		end

		def inspect
			"#<ObjectState @synchronized=%p @version=%p>"%[synchronized_us_ago, version]
		end

	end

end
