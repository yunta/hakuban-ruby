require 'hakuban/ffi-object.rb'
require 'hakuban/stream.rb'


module Hakuban

	class ObjectStateSink < FFIObject

		private_class_method :new


		def initialize_from_ffi_pointer(pointer)
			super(pointer, :hakuban_object_state_sink_drop, nil)
		end


		include Stream


		def next(drop_at_the_end_of_block=true, &block)
			process_item(
				lambda { |pointer| FFI::hakuban_object_state_sink_next(pointer) },
				lambda { |pointer| FFIObject::from_ffi_pointer(ObjectStateSinkParams, pointer) },
				&block
			)
		end


		def send(object_state)
			process_item(
				lambda { |pointer| object_state.with_pointer { |object_state_pointer| FFI::hakuban_object_state_sink_send(pointer, object_state_pointer) } },
				lambda { |_pointer| nil },
				&lambda {}
			)
		end


		def descriptor
			Hakuban::FFIObject.from_ffi_pointer(Hakuban::ObjectDescriptor, with_pointer { |pointer| FFI::hakuban_object_state_sink_descriptor(pointer) })
		end
		

	end

	class ObjectStateSinkParams < FFIObject

		private_class_method :new

		def initialize_from_ffi_pointer(pointer)
			super(pointer, :hakuban_object_state_sink_params_drop, :hakuban_object_state_sink_params_clone)
		end

	end


end