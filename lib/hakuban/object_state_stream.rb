require 'hakuban/ffi.rb'
require 'hakuban/ffi-object.rb'
require 'hakuban/stream.rb'
require 'hakuban/refinements.rb'

module Hakuban


	class ObjectStateStream < FFIObject

		private_class_method :new
		

		def initialize_from_ffi_pointer(pointer)
			super(pointer, :hakuban_object_state_stream_drop, nil)
		end



		include Stream


		
		def next(&block)
			process_item(
				lambda { |pointer| FFI::hakuban_object_state_stream_next(pointer) },
				lambda { |pointer| FFIObject::from_ffi_pointer(ObjectState, pointer) },
				&block
			)
		end
		

		def descriptor
			Hakuban::FFIObject.from_ffi_pointer(Hakuban::ObjectDescriptor, with_pointer { |pointer| FFI::hakuban_object_state_stream_descriptor(pointer) })
		end
		
	end

end