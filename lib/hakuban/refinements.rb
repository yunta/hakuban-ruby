module ThreadExt

	refine Thread do

		def join_with_warning(timeout=60)
			loop {
				self.join(60)
				return self.value  if !self.status
				$stderr.puts "Thread doesn't want to die: \n"+item_thread.backtrace.inspect+"\nat:\n"+caller.join("\n")
			}
		end


		def Thread.process_interrupts
			Thread.handle_interrupt(Object => :immediate) {
				Thread.pass
			}
			nil
		end

	end 

end