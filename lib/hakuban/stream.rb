require 'hakuban/refinements'

#TODO: bring back Async variants

module Hakuban

	module Stream

		using ThreadExt

		class NextItemInterrupt < Exception
			def ==(other)
				other.object_id == self.object_id
			end
		end


		# next may be called in CLI context, so, every return branch should process interrupts explicitly
		private def process_item(future_constructor, item_constructor, &block)
			Thread.handle_interrupt(Object => :never) do
				future_pointer = begin
					with_pointer(&future_constructor)
				rescue FFIObject::PointerAlreadyDropped
					Thread.process_interrupts
					return nil
				end
				
				result, error = Hakuban::FFI::FFIFuture.await(future_pointer)
				item = if pointer = result.unwrap
					item_constructor.call(pointer)
				end

				raise error  if error
				return item   if block.nil?

				if item.nil?
					Thread.process_interrupts
					nil
				else
					Thread.handle_interrupt(Object => :immediate) {
						block.call(item)
					}
				end

			ensure
				item.drop  if !!block and !!item
			end
		end

		def each(*args, **kwargs, &block)
			for_each(*args, **kwargs, &block)
		end

		def for_each(*args, **kwargs, &block)
			if kwargs[:interrupt_on_next]
				for_each_till_next(*args, **kwargs, &block)
			else
				while self.next { |new_item| sync_call(new_item, args, kwargs, block); true }; end
			end
		end


		def for_each_till_next(*args, **kwargs, &block)
			for_each_in_thread(*args,**kwargs.merge(kill_previous_on_next: true), &block)			
		end


		def for_each_concurrent(*args, **kwargs, &block)
			for_each_in_thread(*args, **kwargs, &block)
		end


		def for_each_in_thread(*args, **kwargs, &block)			
			Thread.handle_interrupt(Object => :never) {

				# we poll the stream in a separate thread because we sometimes want to interrupt the polling thread from an item-handling thread, and, without the wrapper-thread, we could end up raising exception in a random place of code, if Async is being used. maybe?
				stream_polling_thread = Thread.new {
					begin
						item_threads = ObjectSpace::WeakMap.new
						exception_to_rescue = NextItemInterrupt.new

						while new_item = self.next
							if kwargs[:kill_previous_on_next]
								item_threads.keys.each { |item_thread| 
									item_thread.raise(exception_to_rescue)
									item_thread.join
								}
							end

							item_threads[Thread.new(new_item, Thread.current) do |item, parent_thread|
								sync_call(item, args, kwargs, block)
							rescue Object => error
								if error != exception_to_rescue
									if kwargs[:propagate_exceptions].nil? or kwargs[:propagate_exceptions]
										parent_thread.raise(error)
									else
										raise
									end
								end
							ensure
								item_threads.delete Thread.current
							end] = true
						end

						if kwargs[:kill_previous_on_next]
							item_threads.keys.each { |item_thread| 
								item_thread.raise(exception_to_rescue)
								item_thread.join
							}
						end

						Thread.handle_interrupt(Object => :immediate) {
							item_threads.keys.each { |item_thread|
								item_thread.join
							}
						}

						nil
					rescue Object => error
						exception_to_rescue = error
						item_threads.keys.each { |item_thread| item_thread.raise(error) }
						error
					ensure
						item_threads.keys.each { |item_thread|
							begin
								item_thread.join_with_warning(60)
							rescue Object => error
								# it's probably better to not re-raise here, so this one doesn't obscure the original. but lets at least print it out.
								$stderr.puts "Item thread exception: \n"+error.inspect
							end
						}	
					end
				}
				 
				begin
					Thread.handle_interrupt(Object => :immediate) {
						value = stream_polling_thread.join.value
						raise value  if !value.nil?
					}
				rescue Object => error
					stream_polling_thread.raise(error)
					raise
				ensure
					begin
						stream_polling_thread.join_with_warning(60)
					rescue Object => error
						# it's probably better to not re-raise here, so this one doesn't obscure the original. but lets at least print it out.
						$stderr.puts "Item thread exception: \n"+error.inspect
					end
				end

			}
		end


		## blocks can't be sanely passed to ractors :(
		# def for_each_in_ractor(*args, **kwargs, &block)
		# 	Thread.handle_interrupt(Object => :never) {
		# 		begin
		# 			Thread.handle_interrupt(Object => :immediate) {
		# 				while new_item = self.next
		# 					Ractor.new(new_item, args, kwargs, runner ) { |item, args, kwargs, runner|
		# 						sync_call(item, args, kwargs, runner)
		# 					}
		# 				end
		# 			}
		# 		ensure
		# 			#TODO: should we kill still-running sub-tasks here, or await? or maybe kill on exception, and await otherwise?
		# 		end
		# 	}
		# end
				

		private def sync_call(item, args, kwargs, block)
			Thread.handle_interrupt(Object => :never) {
				begin
					Thread.handle_interrupt(Object => :immediate) {
						block.call(item, *args, **kwargs)
					}
				ensure
					item.drop
				end
			}
		end


	end

end