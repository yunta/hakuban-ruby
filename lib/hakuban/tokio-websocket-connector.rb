require 'hakuban/ffi-object.rb'

module Hakuban

	class Tokio

		@@pointer = nil

		def Tokio.init(workers_count=0)
			Hakuban::hakuban_initialize
			@@pointer ||= FFI::hakuban_tokio_init_multi_thread(0)
		end

		def Tokio.pointer
			Tokio.init  if not @@pointer
			@@pointer
		end

	end


	class WebsocketConnector < FFIObject

		def initialize(exchange, url, &block)
			super()
			Thread.handle_interrupt(Object => :never) {
				pointer = exchange.with_pointer { |exchange_pointer| FFI::hakuban_tokio_websocket_connector_new(Tokio.pointer, exchange_pointer, url) }.unwrap
				initialize_pointer(pointer, :hakuban_tokio_websocket_connector_drop, nil)
				self.do_and_drop_or_return(&block)
			}
		end

	end

end

def self.default_name
	"#{Socket.gethostname}:#{File.basename(caller_locations(0..1)[1].path)}:#{$$}"
end
