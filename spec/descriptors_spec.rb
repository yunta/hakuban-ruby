require 'hakuban'


RSpec.describe Hakuban::TagDescriptor do

	it "can be created and dropped" do
		tag = Hakuban::TagDescriptor.new("xxx")
		expect(tag.json).to eq "xxx"
	end


	it "doesn't leak on clone" do
		tag = Hakuban::TagDescriptor.new("xxx")
		1.times {
			tag.clone
		}
		#TODO: check rss
	end

	it "can be passed to and from ractor" do
		ractor = Ractor.new("ractor1") {
			descriptor = Ractor.receive
			pointer = descriptor.instance_variable_get("@pointer")
			Ractor.yield(descriptor)
			Ractor.yield(pointer)
		}
		descriptor = Hakuban::TagDescriptor.new("xxx")
		ractor.send(descriptor)
		descriptor_from_ractor = ractor.take()
		descriptor_from_ractor_pointer = ractor.take()
		expect(descriptor).to eq descriptor_from_ractor
		expect(descriptor_from_ractor_pointer).not_to eq descriptor.instance_variable_get("@pointer")
		expect(descriptor_from_ractor_pointer).not_to eq descriptor_from_ractor.instance_variable_get("@pointer")
		expect(descriptor.instance_variable_get("@pointer")).not_to eq descriptor_from_ractor.instance_variable_get("@pointer")
	end

end


RSpec.describe Hakuban::ObjectDescriptor do

	it "can be created and dropped" do
		object = Hakuban::ObjectDescriptor.new([1,"bbb"], "xxx")
	end


	it "can be retrieved through ffi" do
		exchange = Hakuban::Exchange.new
		object_observe_contract = exchange.object_observe_contract(["a","b"],"xxx").build
		object_expose_contract = exchange.object_expose_contract(["a","b"],"xxx").build
		object_state_sink = object_expose_contract.next
		descriptor = object_state_sink.descriptor
		expect(descriptor.json).to eq "xxx"
		expect(descriptor.tags.map { |tag| tag.json }.sort).to eq ["a","b"] 
	end


	# it "doesn't leak on clone" do
	# 	descriptor = Hakuban::ObjectDescriptor.new(["a","b"],"xxx")
	# 	1000000.times {
	# 		descriptor.clone
	# 	}
	# 	#TODO: check rss
	# end

	it "can be passed to ractor" do
		ractor = Ractor.new("ractor1") { Ractor.receive }
		descriptor = Hakuban::ObjectDescriptor.new(["a","b"], "xxx")
		ractor.send(descriptor)
	end

	it "can be created with multiple tags" do
		tag1 = Hakuban::TagDescriptor.new("tag1")
		tag2 = Hakuban::TagDescriptor.new("tag2")
		tag3 = Hakuban::TagDescriptor.new("tag3")
		object = Hakuban::ObjectDescriptor.new([tag1, tag2, tag3], "xxx")
	end

	it "can be compared" do
		object1 = Hakuban::ObjectDescriptor.new([1,"bbb"], "xxx")
		object2 = Hakuban::ObjectDescriptor.new(["bbb",1], "xxx")
		expect(object1).to eq object2
		object3 = Hakuban::ObjectDescriptor.new(["bbb",1], "xxy")
		expect(object1).not_to eq object3
		object4 = Hakuban::ObjectDescriptor.new(["bbb",1,2], "xxx")
		expect(object1).not_to eq object4
	end

	it "can be hashed" do
		object1 = Hakuban::ObjectDescriptor.new([1,"bbb"], "xxx")
		object2 = Hakuban::ObjectDescriptor.new(["bbb",1], "xxx")
		expect(object1.hash).to eq object2.hash
		object3 = Hakuban::ObjectDescriptor.new(["bbb",1], "xxy")
		expect(object1.hash).not_to eq object3.hash
		object4 = Hakuban::ObjectDescriptor.new(["bbb",1,2], "xxx")
		expect(object1.hash).not_to eq object4.hash
	end

	it "can be inspected" do
		object1 = Hakuban::ObjectDescriptor.new([1,"bbb"], "xxx")
		expect(object1.inspect).to match /ObjectDescriptor/
	end

end