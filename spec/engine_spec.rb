require 'hakuban'

Thread.report_on_exception = false

RSpec.describe Hakuban::Engine do



	it "can be created, used, and stopped" do

		exchange = Hakuban::Exchange.new

		class MyBrokenEngine < Hakuban::Engine
		end

		class MyCorrectEngine < Hakuban::Engine
			def initialize
				@test = 1
			end
			def contract(exchange)
				exchange.object_observe_contract([],0).build
			end
			def handle_with_thread(stream)
				stream.for_each {}
			end
		end

		expect(Hakuban::Engine.engines.size).to eq 2
		Hakuban::Engine.engines.each { |engine|
			instance = engine.new
			if engine == MyBrokenEngine 
				expect { instance.run(exchange) }.to raise_error(NoMethodError)
			else
				thread = Thread.new { instance.run(exchange) }
				sleep 1
				Hakuban::Engine.stop(thread)
			end
		}

	end

	it "prints error if one occurs in an engine" do

		exchange = Hakuban::Exchange.new
        
		class FaultyObserveEngine < Hakuban::Engine
			def initialize
			end
			def contract(exchange)
				exchange.object_observe_contract([],0).build
			end
			def handle_with_thread(stream)
				sleep 0.1
				raise Exception.new("fault")

			end
		end

		backup = $stderr
		r, $stderr = IO.pipe
		$stderr.sync = true
		thread = Thread.new { FaultyObserveEngine.new.run(exchange) }
		thread.join
		expect(r.read_nonblock(1000)).to match /fault/
		$stderr = backup
		Hakuban::Engine.stop(thread)
		thread.join

	end

end