require 'hakuban'


RSpec.describe Hakuban::Exchange do

	it "can be created and dropped" do
		exchange = Hakuban::Exchange.new
	end

	it "can be used to observe an object" do
		exchange = Hakuban::Exchange.new
		contract1 = exchange.object_observe_contract(["a"], "b").build
		descriptor = Hakuban::ObjectDescriptor.new(["a"], "b")
		contract2 = exchange.object_observe_contract(descriptor).build
		expect(contract1.descriptor).to eq contract2.descriptor
	end

	it "can be used to observe a tag" do
		exchange = Hakuban::Exchange.new
		contract1 = exchange.tag_observe_contract("b").build
		descriptor = Hakuban::TagDescriptor.new("b")
		contract2 = exchange.tag_observe_contract(descriptor).build
		expect(contract1.descriptor).to eq contract2.descriptor
	end

	it "can be inspected" do
		exchange = Hakuban::Exchange.new
		expect(exchange.inspect).to match /Exchange/
	end

	it "can be copied to and from ractor" do
		ractor = Ractor.new("ractor1") {
			exchange = Ractor.receive
			pointer = exchange.instance_variable_get("@pointer")
			Ractor.yield(exchange)
			Ractor.yield(pointer)
		}
		exchange = Hakuban::Exchange.new
		ractor.send(exchange)
		exchange_from_ractor = ractor.take()
		exchange_from_ractor_pointer = ractor.take()
		expect(exchange_from_ractor_pointer).not_to eq exchange.instance_variable_get("@pointer")
		expect(exchange_from_ractor_pointer).not_to eq exchange_from_ractor.instance_variable_get("@pointer")
		expect(exchange.instance_variable_get("@pointer")).not_to eq exchange_from_ractor.instance_variable_get("@pointer")
	end

	it "keeps refering to the same shared exchange after copy to ractor" do
		ractor = Ractor.new("ractor1") {
			exchange = Ractor.receive
			object_expose = exchange.object_expose_contract([], "xxx").build
			object_state_sink = object_expose.next
			object_state_sink_params = object_state_sink.next
			raise if object_state_sink_params.nil?
			raise unless object_state_sink.descriptor.json == "xxx"
			object_state_sink.send(Hakuban::ObjectState.new("data"))
			object_state_sink_params = object_state_sink.next
			raise unless object_state_sink_params.nil?
		}
		exchange = Hakuban::Exchange.new
		ractor.send(exchange)
		object_observe = exchange.object_observe_contract([], "xxx").build
		object_state_stream = object_observe.next
		state = object_state_stream.next
		object_observe.drop
	end

end
