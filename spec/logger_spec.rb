require 'hakuban'


RSpec.describe Hakuban do

	it "can't be initialized twice" do
		expect { 
			Hakuban.logger_initialize("debug"); Hakuban.logger_initialize("debug")
		}.to raise_error(RuntimeError)
	end

end