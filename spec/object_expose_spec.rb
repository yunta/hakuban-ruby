require 'hakuban'


RSpec.describe Hakuban::ObjectExposeContract do

	it "can be created and dropped" do
		exchange = Hakuban::Exchange.new
		object_expose = exchange.object_expose_contract([],"xxx").build
		object_expose.drop
	end


	it "receives events" do
		exchange = Hakuban::Exchange.new
		
		thread = Thread.new {
			object_observe = exchange.object_observe_contract([], "xxx").build
			object_state_stream = object_observe.next
			assert_order 1
			state = object_state_stream.next
			assert_order 4
			sleep 2
			assert_order 6
			object_observe.drop
			object_state_stream.drop			
		}

		sleep 0.2
		assert_order 2
		object_expose = exchange.object_expose_contract([], "xxx").build

		object_state_sink = object_expose.next
		assert_order 3
		object_state_sink_params = object_state_sink.next
		expect(object_state_sink_params).not_to be nil
		expect(object_state_sink.descriptor.json).to eq "xxx"
		object_state_sink.send(Hakuban::ObjectState.new("data"))
		sleep 1
		assert_order 5
		object_state_sink_params = object_state_sink.next
		expect(object_state_sink_params).to be nil
		assert_order 7
		thread.join
	end

	it "can be desynchronized" do
		exchange = Hakuban::Exchange.new
		
		handover = Queue.new

		thread = Thread.new {
			object_observe = exchange.object_observe_contract([], "xxx").build
			object_state_stream = object_observe.next
			state = object_state_stream.next
			expect(state.synchronized_us_ago).to eq 0
			handover.pop
			state = object_state_stream.next
			expect(state.synchronized_us_ago).to be > 100000
			object_observe.drop
			object_state_stream.drop			
		}

		object_expose = exchange.object_expose_contract([], "xxx").build
		object_state_sink = object_expose.next
		object_state_sink.send(Hakuban::ObjectState.new("data", version: [1]))
		sleep 0.1
		object_state_sink.drop
		sleep 0.1
		handover.push true
		thread.join
	end

	it "receives stream termination on contract termination" do  
		exchange = Hakuban::Exchange.new
		object_expose = exchange.object_expose_contract([], "xxx").build		

		thread = Thread.new {
			assert_order 1
			object_state_sink = object_expose.next
			assert_order 3
			expect(object_state_sink).to be_nil
		}
		sleep 0.1
		assert_order 2
		object_expose.drop
		sleep 0.1
		assert_order 4
		thread.join
	end

end