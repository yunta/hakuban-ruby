require 'hakuban'

Thread::abort_on_exception=true

RSpec.describe Hakuban::ObjectObserveContract do

	it "can be created and dropped" do
		exchange = Hakuban::Exchange.new
		object_observe = exchange.object_observe_contract([],"xxx").build
		object_observe.drop
	end

	it "can be inspected" do
		exchange = Hakuban::Exchange.new
		object_observe = exchange.object_observe_contract([],"xxx").build
		expect(object_observe.inspect).to match /ObjectObserveContract/
	end

	it "can successfully retrieve object state via promise" do
		exchange = Hakuban::Exchange.new
		object_observe_contract = exchange.object_observe_contract([],"xxx").build
		object_expose_contract = exchange.object_expose_contract([], "xxx").build
		object_state_sink = object_expose_contract.next
		new_state = Hakuban::ObjectState.new("data", version: [1,2], format: ["t1"])
		object_state_sink.send(new_state)
		stream = object_observe_contract.next
		state = stream.next
		expect(state.data).to eq "data"
		expect(state.format).to eq ["t1"]
		expect(state.version).to eq [1,2]
		expect(state.synchronized_us_ago).to eq 0
		descriptor = stream.descriptor
		expect(descriptor.json).to eq "xxx"
	end

	it "can successfully retrieve object state, using blocks" do
		Hakuban::Exchange.new { |exchange|
			t1 = Thread.new {
				exchange.object_observe_contract([],"xxx").build { |object_observe_contract|
					object_observe_contract.next { |stream|
						stream.next { |state|
							expect(state.data).to eq "data"
							expect(state.format).to eq ["t1"]
							expect(state.version).to eq [1,2]
							expect(state.synchronized_us_ago).to eq 0
						}
						stream.next { |state|
							expect(state.data).to eq "data2"
							expect(state.format).to eq ["t2"]
							expect(state.version).to eq [1,3]
							expect(state.synchronized_us_ago).to eq 0
						}
						stream.next { |state| 
							expect(state.synchronized_us_ago).to be > 0
						}
					}
				}
			}
		 	t2 = Thread.new {
				exchange.object_expose_contract([], "xxx").build { |object_expose_contract|
					object_expose_contract.next { |sink|
						sink.send Hakuban::ObjectState.new("data", version: [1,2], format: ["t1"])
						sleep 1
						sink.send Hakuban::ObjectState.new("data2", version: [1,3], format: ["t2"])
					}
				}
			}
			t2.join
			t1.join
		}
	end

	it "can successfully expose and retrieve json serialized data" do
		exchange = Hakuban::Exchange.new
		object_observe_contract = exchange.object_observe_contract([], "xxx").build
		object_expose_contract = exchange.object_expose_contract([], "xxx").build
		object_state_sink = object_expose_contract.next
		new_state = Hakuban::ObjectState.new({"data": 1}).with_format("t1").with_version([1,2]).with_synchronized_us_ago(1000000)
		object_state_sink.send(new_state.json_serialize)
		stream = object_observe_contract.next
		state = stream.next.json_deserialize
		expect(state.data).to eq({"data" => 1})
		expect(state.format).to eq ["t1"]
		expect(state.version).to eq [1,2]
		expect(state.synchronized_us_ago).to be > 1000000
		descriptor = stream.descriptor
		expect(descriptor.json).to eq "xxx"
	end


	#TODO: move this to separate file
	it "ObjectState can be created on rust side" do
		new_state = Hakuban::ObjectState.new("data", version: [1])
		new_state.with_pointer { |pointer| expect(pointer).to be_a_kind_of(FFI::Pointer) }
	end

	it "receives events" do  
		exchange = Hakuban::Exchange.new
		
		thread = Thread.new {
			object_observe = exchange.object_observe_contract([], "xxx").build
			object_state_stream = object_observe.next
			assert_order 1
			state = object_state_stream.next
			assert_order 4
			sleep 2
			assert_order 6
			state = object_state_stream.next
			expect(state.synchronized_us_ago).to be > 0
		}

		sleep 0.2
		assert_order 2
		object_expose = exchange.object_expose_contract([], "xxx").build

		object_state_sink = object_expose.next
		assert_order 3

		expect(object_state_sink.descriptor.json).to eq "xxx"
		object_state_sink.send(Hakuban::ObjectState.new("data"))
		sleep 1
		assert_order 5
		object_expose.drop
		object_state_sink.drop
		sleep 2
		thread.join
	end

	it "receives stream termination on contract termination" do  
		exchange = Hakuban::Exchange.new
		object_observe = exchange.object_observe_contract([], "xxx").build		

		thread = Thread.new {
			assert_order 1
			object_state_stream = object_observe.next
			assert_order 2
			expect(object_state_stream.next).to be_nil
		}

		sleep 0.2
		assert_order 3
		object_observe.drop
		sleep 0.1
		assert_order 4
		thread.join
	end

end