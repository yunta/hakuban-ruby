require 'hakuban'


RSpec.describe Hakuban::ObjectState do

	it "can be created and modified" do
		state = Hakuban::ObjectState.new({"data" => 1}).with_format("t1").with_version([1,2]).with_synchronized_us_ago(1000000)
		expect(state.data).to eq({"data" => 1})
		expect(state.format).to eq ["t1"]
		expect(state.version).to eq [1,2]
		expect(state.synchronized_us_ago).to eq 1000000
		state = state.with_data("b").with_format(["a","b"]).with_version([0]).with_synchronized_us_ago(0)
		expect(state.data).to eq("b")
		expect(state.format).to eq ["a","b"]
		expect(state.version).to eq [0]
		expect(state.synchronized_us_ago).to eq 0
	end

	it "can be inspected" do
		state = Hakuban::ObjectState.new({"data" => 1}).with_format("t1").with_version([1,2]).with_synchronized_us_ago(1000000)
		expect(state.inspect).to match /ObjectState/
	end

end