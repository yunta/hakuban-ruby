require 'hakuban'
include Hakuban

COUNT=10000

exchange = Exchange.new
#connector = WebsocketConnector.new(exchange, "ws://localhost:3001#diff-request=false,diff-produce=false")
connector = WebsocketConnector.new(exchange, "ws://localhost:3001#nodelay=true")
object_observe_contract = exchange.object_observe_contract([], ARGV[0]).build
object_expose_contract = exchange.object_expose_contract([], ARGV[1]).build

object_state_sink = object_expose_contract.next

puts "Starting"
last_received_version = -1
object_state_stream = nil
started_at = Time.new
t_send = 0
t_receive = 0
COUNT.times { |i|
	#print "."
	t = Time.new
	object_state_sink.send(ObjectState.new([i], "data#{i}"))
	t_send += Time.new - t
	object_state_stream ||= object_observe_contract.next
	while last_received_version < i
		t = Time.new
		last_received_version = object_state_stream.next.version[0]
		t_receive += Time.new - t
	end

}
t_total = Time.new - started_at
puts "Send time #{t_send}, Receive time #{t_receive}, Total time: #{t_total}, Per iteration: #{t_total/COUNT}, Loops per second: #{COUNT/t_total}"

