require 'hakuban'
require 'timeout'



class StopBlock < Exception; end

RSpec.describe Hakuban::ObjectObserveContract do

	it "is always dropped if built with a block, even when thread gets killed at random times" do
		Thread::abort_on_exception=false
		Thread::report_on_exception=false

		iterations = 10000
		exchange = Hakuban::Exchange.new
		threads = Queue.new
		spawner = Thread.new {
			iterations.times {
				threads.push Thread.new {
					Thread.handle_interrupt(Object => :never) do
						Thread.handle_interrupt(Object => :immediate) {
							exchange.object_observe_contract([],"xxx").build {
								sleep 0.1
							}
						}
					rescue StopBlock
					end
				}
			}
		}
		killer = Thread.new {
			iterations.times {
				threads.pop.raise StopBlock
			}
		}
		killer.join
		spawner.join
		expect {
			Timeout::timeout(1) {
				exchange.object_expose_contract([],"xxx").build { |sink_stream|
					sink_stream.next
				}
			}
		}.to raise_error(Timeout::Error)
	end

end