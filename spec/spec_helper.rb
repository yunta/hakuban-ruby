# frozen_string_literal: true


require 'simplecov'
SimpleCov.start do
  skip_token 'unreachable'
end

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:each) do
    @next_expected_order = 1
  end

  def assert_order(number)
    raise "Expected assert_order(#{@next_expected_order}) to be called, but assert_order(#{number}) was called."  if not @next_expected_order == number
    @next_expected_order += 1
  end

end 


