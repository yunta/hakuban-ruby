require 'hakuban'
#require 'async'

Thread::abort_on_exception=true

RSpec.describe Hakuban::Stream do

	it "can be used to iterate over items synchronously     ,         dropping items" do
		exchange = Hakuban::Exchange.new

		go = Queue.new

		t1 = Thread.new {
			exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
				object_expose_contract.next { |object_state_sink|
					object_state_sink.send Hakuban::ObjectState.new("d1")
					expect(go.pop).to eq "d1"
					object_state_sink.send Hakuban::ObjectState.new("d2")
					expect(go.pop).to eq "d2"
				}
			}
		}

		t2 = Thread.new {
			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
				tag_observe_contract.next { |object_state_stream|
					previous_state = nil
					object_state_stream.for_each { |state|
						if previous_state
							expect(previous_state.dropped?).to eq true
						end
						if state.data == "d1"
							go.push "d1"
						elsif state.data == "d2"
							go.push "d2"
						else
							# :unreachable:
							raise
							# :unreachable:
						end
						previous_state = state
					}
				}
			}
		}

		t1.join
		t2.join
	end


	it "can be used to iterate over items synchronously     , without dropping items" do
		exchange = Hakuban::Exchange.new

		go = Queue.new

		t1 = Thread.new {
			exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
				object_expose_contract.next { |object_state_sink|
					object_state_sink.send Hakuban::ObjectState.new("d1")
					expect(go.pop).to eq "d1"
					object_state_sink.send Hakuban::ObjectState.new("d2")
					expect(go.pop).to eq "d2"
				}
			}
		}

		t2 = Thread.new {
			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
				tag_observe_contract.next { |object_state_stream|
					previous_state = nil
					while state = object_state_stream.next 
						if previous_state
							expect(previous_state.dropped?).to eq false
						end
						if state.data == "d1"
							go.push "d1"
						elsif state.data == "d2"
							go.push "d2"
						else
							# :unreachable:
							raise
							# :unreachable:
						end
						previous_state = state
					end
				}
			}
		}

		t1.join
		t2.join
	end


	# it "can be used to iterate over items with Async  blocks,         dropping items, not killing tasks" do
	# 	exchange = Hakuban::Exchange.new

	# 	go = Queue.new

	# 	t1 = Thread.new {
	# 		exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
	# 			object_expose_contract.next { |object_state_sink|
	# 				object_state_sink.send Hakuban::ObjectState.new("d1")
	# 				expect(go.pop).to eq "d1"
	# 				object_state_sink.send Hakuban::ObjectState.new("d2")
	# 				expect(go.pop).to eq "d2"
	# 				object_state_sink.send Hakuban::ObjectState.new("end")
	# 				expect(go.pop).to eq "end"
	# 			}
	# 		}
	# 	}

	# 	t2 = Thread.new {
	# 		Async {
	# 			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
	# 				tag_observe_contract.next { |object_state_stream|
	# 					d1_state = nil
	# 					d2_state = nil
	# 					d1_task = nil
	# 					d1_task_go = Queue.new
	# 					d2_task = nil
	# 					d2_task_go = Queue.new
	# 					while state = object_state_stream.next 
	# 						Async {
	# 							state.do_and_maybe_drop_or_return(true) {
	# 								if state.data == "d1"
	# 									d1_state = state
	# 									d1_task = :running
	# 									go.push "d1"
	# 									d1_task_go.pop
	# 									d1_task = :exit
	# 								elsif state.data == "d2"
	# 									d2_state = state
	# 									d2_task = :running
	# 									go.push "d2"
	# 									d2_task_go.pop
	# 									d2_task = :exit
	# 								else
	# 									go.push "end"
	# 								end
	# 							}
	# 						}
	# 					end
	# 					expect(d1_state.data).to eq "d1"
	# 					expect(d2_state.data).to eq "d2"
	# 					expect(d1_state.dropped?).to eq false
	# 					expect(d2_state.dropped?).to eq false
	# 					expect(d1_task).to eq :running
	# 					expect(d2_task).to eq :running
	# 					d1_task_go.push true
	# 					sleep 0.1
	# 					expect(d1_state.data).to eq "d1"
	# 					expect(d2_state.data).to eq "d2"
	# 					expect(d1_state.dropped?).to eq true
	# 					expect(d2_state.dropped?).to eq false
	# 					expect(d1_task).to eq :exit
	# 					expect(d2_task).to eq :running
	# 					d2_task_go.push true
	# 					sleep 0.2
	# 					expect(d1_state.data).to eq "d1"
	# 					expect(d2_state.data).to eq "d2"
	# 					expect(d1_state.dropped?).to eq true
	# 					expect(d2_state.dropped?).to eq true
	# 					expect(d1_task).to eq :exit
	# 					expect(d2_task).to eq :exit
	# 				}
	# 			}
	# 		}
	# 	}

	# 	t1.join
	# 	t2.join
	# end

	# it "can be used to iterate over items with Async  blocks, without dropping items, not killing tasks" do
	# 	exchange = Hakuban::Exchange.new

	# 	go = Queue.new

	# 	t1 = Thread.new {
	# 		exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
	# 			object_expose_contract.next { |object_state_sink|
	# 				object_state_sink.send Hakuban::ObjectState.new("d1")
	# 				expect(go.pop).to eq "d1"
	# 				object_state_sink.send Hakuban::ObjectState.new("d2")
	# 				expect(go.pop).to eq "d2"
	# 				object_state_sink.send Hakuban::ObjectState.new("end")
	# 				expect(go.pop).to eq "end"
	# 			}
	# 		}
	# 	}

	# 	t2 = Thread.new {
	# 		Async {
	# 			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
	# 				tag_observe_contract.next { |object_state_stream|
	# 					d1_state = nil
	# 					d2_state = nil
	# 					d1_task = nil
	# 					d1_task_go = Queue.new
	# 					d2_task = nil
	# 					d2_task_go = Queue.new
	# 					while state = object_state_stream.next
	# 						Async {
	# 							if state.data == "d1"
	# 								d1_state = state
	# 								d1_task = :running
	# 								go.push "d1"
	# 								d1_task_go.pop
	# 								d1_task = :exit
	# 							elsif state.data == "d2"
	# 								d2_state = state
	# 								d2_task = :running
	# 								go.push "d2"
	# 								d2_task_go.pop
	# 								d2_task = :exit
	# 							else
	# 								go.push "end"
	# 							end
	# 						}
	# 					end
	# 					expect(d1_state.data).to eq "d1"
	# 					expect(d2_state.data).to eq "d2"
	# 					expect(d1_state.dropped?).to eq false
	# 					expect(d2_state.dropped?).to eq false
	# 					expect(d1_task).to eq :running
	# 					expect(d2_task).to eq :running
	# 					d1_task_go.push true
	# 					sleep 0.1
	# 					expect(d1_state.data).to eq "d1"
	# 					expect(d2_state.data).to eq "d2"
	# 					expect(d1_state.dropped?).to eq false
	# 					expect(d2_state.dropped?).to eq false
	# 					expect(d1_task).to eq :exit
	# 					expect(d2_task).to eq :running
	# 					d2_task_go.push true
	# 					sleep 0.2
	# 					expect(d1_state.data).to eq "d1"
	# 					expect(d2_state.data).to eq "d2"
	# 					expect(d1_state.dropped?).to eq false
	# 					expect(d2_state.dropped?).to eq false
	# 					expect(d1_task).to eq :exit
	# 					expect(d2_task).to eq :exit
	# 				}
	# 			}
	# 		}
	# 	}

	# 	t1.join
	# 	t2.join
	# end


	it "can be used to iterate over items with Thread blocks,         dropping items, not killing tasks" do
		exchange = Hakuban::Exchange.new

		go = Queue.new
		go_check = Queue.new

		t1 = Thread.new {
			exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
				object_expose_contract.next { |object_state_sink|
					object_state_sink.send Hakuban::ObjectState.new("d1")
					expect(go.pop).to eq "d1"
					object_state_sink.send Hakuban::ObjectState.new("d2")
					expect(go.pop).to eq "d2"
					object_state_sink.send Hakuban::ObjectState.new("end")
					expect(go.pop).to eq "end"
					go_check.push true
				}
			}
		}

		t2 = Thread.new {
			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
				tag_observe_contract.next { |object_state_stream|
					d1_state = nil
					d2_state = nil
					d1_task = nil
					d1_task_go = Queue.new
					d2_task = nil
					d2_task_go = Queue.new
					t3 = Thread.new {
						object_state_stream.for_each_concurrent { |state|
							if state.data == "d1"
								d1_state = state
								d1_task = :running
								go.push "d1"
								d1_task_go.pop
								d1_task = :exit
							elsif state.data == "d2"
								d2_state = state
								d2_task = :running
								go.push "d2"
								d2_task_go.pop
								d2_task = :exit
							else
								go.push "end"
							end
						}
					}
					go_check.pop
					expect(d1_state.data).to eq "d1"
					expect(d2_state.data).to eq "d2"
					expect(d1_state.dropped?).to eq false
					expect(d2_state.dropped?).to eq false
					expect(d1_task).to eq :running
					expect(d2_task).to eq :running
					d1_task_go.push true
					sleep 0.1
					expect(d1_state.data).to eq "d1"
					expect(d2_state.data).to eq "d2"
					expect(d1_state.dropped?).to eq true
					expect(d2_state.dropped?).to eq false
					expect(d1_task).to eq :exit
					expect(d2_task).to eq :running
					d2_task_go.push true
					sleep 0.2
					expect(d1_state.data).to eq "d1"
					expect(d2_state.data).to eq "d2"
					expect(d1_state.dropped?).to eq true
					expect(d2_state.dropped?).to eq true
					expect(d1_task).to eq :exit
					expect(d2_task).to eq :exit
					t3.join
				}
			}
		}

		t1.join
		t2.join
	end

	it "can be used to iterate over items with Thread blocks, without dropping items, not killing tasks" do
		exchange = Hakuban::Exchange.new

		go = Queue.new

		t1 = Thread.new {
			exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
				object_expose_contract.next { |object_state_sink|
					object_state_sink.send Hakuban::ObjectState.new("d1")
					expect(go.pop).to eq "d1"
					object_state_sink.send Hakuban::ObjectState.new("d2")
					expect(go.pop).to eq "d2"
					object_state_sink.send Hakuban::ObjectState.new("end")
					expect(go.pop).to eq "end"
				}
			}
		}

		t2 = Thread.new {
			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
				tag_observe_contract.next { |object_state_stream|
					d1_state = nil
					d2_state = nil
					d1_task = nil
					d1_task_go = Queue.new
					d2_task = nil
					d2_task_go = Queue.new
					while state = object_state_stream.next
						Thread.new {
							if state.data == "d1"
								d1_state = state
								d1_task = :running
								go.push "d1"
								d1_task_go.pop
								d1_task = :exit
							elsif state.data == "d2"
								d2_state = state
								d2_task = :running
								go.push "d2"
								d2_task_go.pop
								d2_task = :exit
							else
								go.push "end"
							end
						}
					end
					expect(d1_state.data).to eq "d1"
					expect(d2_state.data).to eq "d2"
					expect(d1_state.dropped?).to eq false
					expect(d2_state.dropped?).to eq false
					expect(d1_task).to eq :running
					expect(d2_task).to eq :running
					d1_task_go.push true
					sleep 0.1
					expect(d1_state.data).to eq "d1"
					expect(d2_state.data).to eq "d2"
					expect(d1_state.dropped?).to eq false
					expect(d2_state.dropped?).to eq false
					expect(d1_task).to eq :exit
					expect(d2_task).to eq :running
					d2_task_go.push true
					sleep 0.2
					expect(d1_state.data).to eq "d1"
					expect(d2_state.data).to eq "d2"
					expect(d1_state.dropped?).to eq false
					expect(d2_state.dropped?).to eq false
					expect(d1_task).to eq :exit
					expect(d2_task).to eq :exit
				}
			}
		}

		t1.join
		t2.join
	end


	# it "can be used to iterate over items with Async  blocks,         dropping items,     killing tasks" do
	# 	exchange = Hakuban::Exchange.new

	# 	go = Queue.new

	# 	t1 = Thread.new {
	# 		exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
	# 			object_expose_contract.next { |object_state_sink|
	# 				object_state_sink.send Hakuban::ObjectState.new("d1")
	# 				expect(go.pop).to eq "d1"
	# 				object_state_sink.send Hakuban::ObjectState.new("d2")
	# 				expect(go.pop).to eq "d2"
	# 				object_state_sink.send Hakuban::ObjectState.new("end")
	# 				expect(go.pop).to eq "end"
	# 			}
	# 		}
	# 	}

	# 	t2 = Thread.new {
	# 		Async {
	# 			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
	# 				tag_observe_contract.next { |object_state_stream|
	# 					d1_state = nil
	# 					d2_state = nil
	# 					d1_task = nil
	# 					d1_task_go = Queue.new
	# 					d2_task = nil
	# 					d2_task_go = Queue.new
	# 					previous_async_to_kill = nil
	# 					while state = object_state_stream.next
	# 						if previous_async_to_kill
	# 							previous_async_to_kill.stop
	# 							previous_async_to_kill.wait
	# 						end
	# 						previous_async_to_kill = Async(state) { |_task, state|
	# 							state.do_and_maybe_drop_or_return(true) { |state|
	# 								if state.data == "d1"
	# 									begin
	# 										d1_state = state
	# 										d1_task = :running
	# 										go.push "d1"
	# 										d1_task_go.pop
	# 										# :unreachable:
	# 										raise
	# 										# :unreachable:
	# 									ensure
	# 										d1_task = :exited
	# 									end
	# 								elsif state.data == "d2"
	# 									begin
	# 										expect(d1_state.data).to eq "d1"
	# 										expect(d1_state.dropped?).to eq true
	# 										expect(d1_task).to eq :exited
	# 										d2_state = state
	# 										d2_task = :running
	# 										go.push "d2"
	# 										d2_task_go.pop
	# 										# :unreachable:
	# 										raise
	# 										# :unreachable:
	# 									ensure
	# 										d2_task = :exited
	# 									end
	# 								else
	# 									go.push "end"
	# 								end
	# 							}
	# 						}
	# 					end
	# 					previous_async_to_kill.stop
	# 					previous_async_to_kill.wait				
	# 					expect(d1_state.data).to eq "d1"
	# 					expect(d2_state.data).to eq "d2"
	# 					expect(d1_state.dropped?).to eq true
	# 					expect(d2_state.dropped?).to eq true
	# 					expect(d1_task).to eq :exited
	# 					expect(d2_task).to eq :exited
	# 				}
	# 			}
	# 		}
	# 	}

	# 	t1.join
	# 	t2.join
	# end


	# it "can be used to iterate over items with Async  blocks, without dropping items,     killing tasks" do
	# 	exchange = Hakuban::Exchange.new

	# 	go = Queue.new

	# 	t1 = Thread.new {
	# 		exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
	# 			object_expose_contract.next { |object_state_sink|
	# 				object_state_sink.send Hakuban::ObjectState.new("d1")
	# 				expect(go.pop).to eq "d1"
	# 				object_state_sink.send Hakuban::ObjectState.new("d2")
	# 				expect(go.pop).to eq "d2"
	# 				object_state_sink.send Hakuban::ObjectState.new("end")
	# 				expect(go.pop).to eq "end"
	# 			}
	# 		}
	# 	}

	# 	t2 = Thread.new {
	# 		Async {
	# 			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
	# 				tag_observe_contract.next { |object_state_stream|
	# 					d1_state = nil
	# 					d2_state = nil
	# 					d1_task = nil
	# 					d1_task_go = Queue.new
	# 					d2_task = nil
	# 					d2_task_go = Queue.new
	# 					previous_async_to_kill = nil
	# 					while state = object_state_stream.next
	# 						if previous_async_to_kill
	# 							previous_async_to_kill.stop
	# 							previous_async_to_kill.wait
	# 						end
	# 						previous_async_to_kill = Async {
	# 							if state.data == "d1"
	# 								begin
	# 									d1_state = state
	# 									d1_task = :running
	# 									go.push "d1"
	# 									d1_task_go.pop
	# 									# :unreachable:
	# 									raise
	# 									# :unreachable:
	# 								ensure
	# 									d1_task = :exited
	# 								end
	# 							elsif state.data == "d2"
	# 								begin
	# 									expect(d1_state.data).to eq "d1"
	# 									expect(d1_state.dropped?).to eq false
	# 									expect(d1_task).to eq :exited
	# 									d2_state = state
	# 									d2_task = :running
	# 									go.push "d2"
	# 									d2_task_go.pop
	# 									# :unreachable:
	# 									raise
	# 									# :unreachable:
	# 								ensure
	# 									d2_task = :exited
	# 								end
	# 							else
	# 								go.push "end"
	# 							end
	# 						}
	# 					end
	# 					previous_async_to_kill.stop
	# 					previous_async_to_kill.wait
	# 					expect(d1_state.data).to eq "d1"
	# 					expect(d2_state.data).to eq "d2"
	# 					expect(d1_state.dropped?).to eq false
	# 					expect(d2_state.dropped?).to eq false
	# 					expect(d1_task).to eq :exited
	# 					expect(d2_task).to eq :exited
	# 				}
	# 			}
	# 		}
	# 	}

	# 	t1.join
	# 	t2.join
	# end




	it "can be used to iterate over items with Thread blocks,         dropping items,     killing tasks" do
		exchange = Hakuban::Exchange.new

		go = Queue.new

		t1 = Thread.new {
			exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
				object_expose_contract.next { |object_state_sink|
					object_state_sink.send Hakuban::ObjectState.new("d1")
					expect(go.pop).to eq "d1"
					object_state_sink.send Hakuban::ObjectState.new("d2")
					expect(go.pop).to eq "d2"
					object_state_sink.send Hakuban::ObjectState.new("end")
					expect(go.pop).to eq "end"
				}
			}
		}

		t2 = Thread.new {
			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
				tag_observe_contract.next { |object_state_stream|
					d1_state = nil
					d2_state = nil
					d1_task = nil
					d1_task_go = Queue.new
					d2_task = nil
					d2_task_go = Queue.new
					object_state_stream.for_each_till_next { |state|
						if state.data == "d1"
							begin
								d1_state = state
								d1_task = :running
								go.push "d1"
								d1_task_go.pop
								# :unreachable:
								raise
								# :unreachable:
							ensure
								d1_task = :exited
							end
						elsif state.data == "d2"
							begin
								expect(d1_state.data).to eq "d1"
								expect(d1_state.dropped?).to eq true
								expect(d1_task).to eq :exited
								d2_state = state
								d2_task = :running
								go.push "d2"
								d2_task_go.pop
								# :unreachable:
								raise
								# :unreachable:
							ensure
								d2_task = :exited
							end
						else
							go.push "end"
						end
					}
					expect(d1_state.data).to eq "d1"
					expect(d2_state.data).to eq "d2"
					expect(d1_state.dropped?).to eq true
					expect(d2_state.dropped?).to eq true
					expect(d1_task).to eq :exited
					expect(d2_task).to eq :exited
				}
			}
		}

		t1.join
		t2.join
	end


	it "can be used to iterate over items with Thread blocks, without dropping items,     killing tasks" do
		exchange = Hakuban::Exchange.new
		class StopBlock < Exception; end

		go = Queue.new

		t1 = Thread.new {
			exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
				object_expose_contract.next { |object_state_sink|
					object_state_sink.send Hakuban::ObjectState.new("d1")
					expect(go.pop).to eq "d1"
					object_state_sink.send Hakuban::ObjectState.new("d2")
					expect(go.pop).to eq "d2"
					object_state_sink.send Hakuban::ObjectState.new("end")
					expect(go.pop).to eq "end"
				}
			}
		}

		t2 = Thread.new {
			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
				tag_observe_contract.next { |object_state_stream|
					d1_state = nil
					d2_state = nil
					d1_task = nil
					d1_task_go = Queue.new
					d2_task = nil
					d2_task_go = Queue.new
					previous_thread_to_kill = nil
					while state = object_state_stream.next
						if previous_thread_to_kill
							previous_thread_to_kill.raise StopBlock
							previous_thread_to_kill.value
						end
						previous_thread_to_kill = Thread.new(state) do |state|
							if state.data == "d1"
								begin
									d1_state = state
									d1_task = :running
									go.push "d1"
									d1_task_go.pop
									# :unreachable:
									raise
									# :unreachable:
								ensure
									d1_task = :exited
								end
							elsif state.data == "d2"
								begin
									expect(d1_state.data).to eq "d1"
									expect(d1_state.dropped?).to eq false
									expect(d1_task).to eq :exited
									d2_state = state
									d2_task = :running
									go.push "d2"
									d2_task_go.pop
									# :unreachable:
									raise
									# :unreachable:
								ensure
									d2_task = :exited
								end
							else
								go.push "end"
							end
						rescue StopBlock
						end
					end
					previous_thread_to_kill.raise StopBlock
					previous_thread_to_kill.value
					expect(d1_state.data).to eq "d1"
					expect(d2_state.data).to eq "d2"
					expect(d1_state.dropped?).to eq false
					expect(d2_state.dropped?).to eq false
					expect(d1_task).to eq :exited
					expect(d2_task).to eq :exited
				}
			}
		}

		t1.join
		t2.join
	end

	it "can pass extra args to block" do
		exchange = Hakuban::Exchange.new

		t1 = Thread.new {
			exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
				object_expose_contract.next { |object_state_sink|
					object_state_sink.send Hakuban::ObjectState.new("d1")
					object_state_sink.for_each {}
				}
			}
		}

		t2 = Thread.new {
			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
				tag_observe_contract.next { |object_state_stream|
					object_state_stream.for_each(1) { |state, a1|
						expect(a1).to eq 1
						tag_observe_contract.drop
					}
				}
			}
		}

		t1.join
		t2.join
	end

	it "can pass extra kwargs to block" do
		exchange = Hakuban::Exchange.new

		t1 = Thread.new {
			exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
				object_expose_contract.next { |object_state_sink|
					object_state_sink.send Hakuban::ObjectState.new("d1")
					object_state_sink.for_each {}
				}
			}
		}

		t2 = Thread.new {
			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
				tag_observe_contract.next { |object_state_stream|
					object_state_stream.for_each(a: 1) { |state, a:|
						expect(a).to eq 1
						tag_observe_contract.drop
					}
				}
			}
		}

		t1.join
		t2.join
	end

	it "can pass extra args and kwargs to block" do
		exchange = Hakuban::Exchange.new

		t1 = Thread.new {
			exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
				object_expose_contract.next { |object_state_sink|
					object_state_sink.send Hakuban::ObjectState.new("d1")
					object_state_sink.for_each {}
				}
			}
		}

		t2 = Thread.new {
			exchange.tag_observe_contract("aaa").build { |tag_observe_contract|
				tag_observe_contract.next { |object_state_stream|
					object_state_stream.for_each(1, a: 2) { |state, b, a:|
						expect(b).to eq 1
						expect(a).to eq 2
						tag_observe_contract.drop
					}
				}
			}
		}

		t1.join
		t2.join
	end


	it "concurrent block can be terminated with exception" do
		class StopBlock < Exception; end

		exchange = Hakuban::Exchange.new

		go = Queue.new

		t1 = Thread.new {
			exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
				object_expose_contract.next { |object_state_sink|
					object_state_sink.send Hakuban::ObjectState.new("d1")
					object_state_sink.for_each {}
				}
			}
		}

		t2 = Thread.new {
			exchange.tag_observe_contract("aaa").build do |tag_observe_contract|
				tag_observe_contract.for_each_concurrent { |object_state_stream|
					go.push true
					object_state_stream.for_each {}
				}
			rescue StopBlock
			end
			nil
		}

		go.pop
		t2.raise StopBlock

		t1.join
		t2.join
	end


	it "item-block exception gets propagated to the thread calling for_each_concurrent" do
		class BlockError < Exception; end

		#Thread.report_on_exception = false
		#Thread.abort_on_exception = false

		exchange = Hakuban::Exchange.new

		go = Queue.new

		t1 = Thread.new {
			exchange.object_expose_contract(["aaa"], "xxx").build { |object_expose_contract|
				object_expose_contract.next { |object_state_sink|
					object_state_sink.send Hakuban::ObjectState.new("d1")
					object_state_sink.for_each {}
				}
			}
		}

		t2 = Thread.new {
			exchange.tag_observe_contract("aaa").build do |tag_observe_contract|
				expect {
						tag_observe_contract.for_each_concurrent { |object_state_stream|
						go.push true
						raise BlockError
						object_state_stream.for_each {}
					}
				}.to raise_error(BlockError)
			end
		}

		go.pop

		t1.join
		t2.join
	end


end
