require 'hakuban'


RSpec.describe Hakuban::TagExposeContract do

	it "can be created and dropped" do
		exchange = Hakuban::Exchange.new
		tag_expose = exchange.tag_expose_contract("xxx").build
		tag_expose.drop
	end


	it "can successfully set object state via promise" do
		exchange = Hakuban::Exchange.new
		object_observe_contract = exchange.object_observe_contract(["yyy"],"xxx").build
		tag_expose_contract = exchange.tag_expose_contract("yyy").build
		object_state_sink = tag_expose_contract.next
		new_state = Hakuban::ObjectState.new("data", version: [1,2], format: ["t1"])
		object_state_sink.send(new_state)
		stream = object_observe_contract.next
		state = stream.next
		expect(state.data).to eq "data"
		expect(state.format).to eq ["t1"]
		expect(state.version).to eq [1,2]
		expect(state.synchronized_us_ago).to eq 0
		descriptor = stream.descriptor
		expect(descriptor.json).to eq "xxx"
	end

	
	it "receives stream termination on contract termination" do  
		exchange = Hakuban::Exchange.new
		tag_expose = exchange.tag_expose_contract("xxx").build		

		thread = Thread.new {
			assert_order 1
			object_state_sink = tag_expose.next
			assert_order 3
			expect(object_state_sink).to be_nil
		}

		sleep 0.1
		assert_order 2
		tag_expose.drop
		sleep 0.1
		assert_order 4
		thread.join
	end


end