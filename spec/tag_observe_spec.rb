require 'hakuban'


RSpec.describe Hakuban::TagObserveContract do

	it "can be created and dropped" do
		exchange = Hakuban::Exchange.new
		tag_expose = exchange.tag_observe_contract("xxx").build
		tag_expose.drop
	end
	

	it "can successfully retrieve object state via promise" do
		exchange = Hakuban::Exchange.new
		tag_observe_contract = exchange.tag_observe_contract("yyy").build
		object_expose_contract = exchange.object_expose_contract(["yyy"], "xxx").build
		object_state_sink = object_expose_contract.next
		new_state = Hakuban::ObjectState.new("data", version: [1,2], format: ["t1"])
		object_state_sink.send(new_state)
		stream = tag_observe_contract.next
		state = stream.next
		expect(state.data).to eq "data"
		expect(state.format).to eq ["t1"]
		expect(state.version).to eq [1,2]
		expect(state.synchronized_us_ago).to eq 0
		descriptor = stream.descriptor
		expect(descriptor.json).to eq "xxx"
	end


	it "receives stream termination on contract termination" do  
		exchange = Hakuban::Exchange.new
		tag_observe = exchange.tag_observe_contract("xxx").build		

		thread = Thread.new {
			assert_order 1
			object_state_stream = tag_observe.next
			assert_order 3
			expect(object_state_stream).to be_nil
		}

		sleep 0.1
		assert_order 2
		tag_observe.drop
		sleep 0.1
		assert_order 4
		thread.join
	end

end