require 'hakuban'


RSpec.describe Hakuban::ObjectObserveContract do

	it "WebsocketConnector raises error when given invalid URL" do
		exchange = Hakuban::Exchange.new()
		expect { connector = Hakuban::WebsocketConnector.new(exchange, "") }.to raise_error(Hakuban::FFI::FFIErrorInvalidURL)
	end

	it "WebsocketConnector can be created and dropped" do
		exchange = Hakuban::Exchange.new()
		connector = Hakuban::WebsocketConnector.new(exchange, "ws://localhost:3001")
		connector.drop
	end

end